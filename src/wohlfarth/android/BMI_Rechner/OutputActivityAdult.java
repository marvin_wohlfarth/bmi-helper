package wohlfarth.android.BMI_Rechner;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.content.Intent;
import android.widget.Toast;


/**
 * Created by marvin.wohlfarth on 25.03.2014.
 */
public class OutputActivityAdult extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.output_result_layout_adult);

        final Button save = (Button) findViewById(R.id.btn_save);
        final TextView resultAge = (TextView) findViewById(R.id.result_age);


        getActionBar().setDisplayHomeAsUpEnabled(true);
        final TextView output = (TextView) findViewById(R.id.output);
        final TextView overview = (TextView) findViewById(R.id.overview);
        Intent i = getIntent();
        final String outputResult = i.getStringExtra("Result");
        output.setText(R.string.output_bmi + outputResult);
        String gender = i.getStringExtra("Gender");
        final String ageTemp = i.getStringExtra("Age");
        final int age = Integer.parseInt(ageTemp);
        if (age >= 19 && age < 25) {
            resultAge.setText("optimaler BMI: 19-24");
        } else if (age >= 25 && age < 35) {
            resultAge.setText("optimaler BMI: 20-25");
        } else if (age >= 35 && age < 45) {
            resultAge.setText("optimaler BMI: 21-26");
        } else if (age >= 45 && age < 55) {
            resultAge.setText("optimaler BMI: 22-27");
        } else if (age >= 55 && age < 65) {
            resultAge.setText("optimaler BMI: 23-28");
        } else if (age >= 65) {
            resultAge.setText("optimaler BMI: 24-29");
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences("MySaves", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("key", outputResult);
                editor.apply();
                Toast.makeText(getApplicationContext(), R.string.save_value, Toast.LENGTH_SHORT).show();
            }
        });


        if (gender.equals("männlich")) {
            double bmiWert = Double.parseDouble(outputResult);

            if (bmiWert < 20.0) {
                overview.setText("Untergewicht");
            } else if (bmiWert >= 20.0 & bmiWert < 26.0) {
                overview.setText("Normalgewicht");
            } else if (bmiWert >= 26.0 & bmiWert <= 31.0) {
                overview.setText("Übergewicht");

            } else if (bmiWert > 31.0 & bmiWert <= 40) {
                overview.setText("Adipositas");
            } else {
                overview.setText("starke Adipositas");
            }
        } else if (gender.equals("weiblich")) {
            double bmiWert = Double.parseDouble(outputResult);

            if (bmiWert < 19.0) {
                overview.setText("Untergewicht");
            } else if (bmiWert >= 19.0 & bmiWert < 25.0) {
                overview.setText("Normalgewicht");
            } else if (bmiWert >= 25.0 & bmiWert < 31.0) {
                overview.setText("Übergewicht");

            } else if (bmiWert >= 31.0 & bmiWert <= 40) {
                overview.setText("Adipositas");
            } else {
                overview.setText("starke Adipositas");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
