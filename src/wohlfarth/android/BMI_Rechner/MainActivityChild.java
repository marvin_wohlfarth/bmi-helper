package wohlfarth.android.BMI_Rechner;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.*;

/**
 * Created by marvin.wohlfarth on 04.04.2014.
 */
public class MainActivityChild extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_child);
        getActionBar().setDisplayHomeAsUpEnabled(true);

        final RadioButton male = (RadioButton) findViewById(R.id.radio_male);
        final RadioButton female = (RadioButton) findViewById(R.id.radio_female);
        final Button calculate = (Button) findViewById(R.id.btn_calc);
        final EditText weight = (EditText) findViewById(R.id.input_weight);
        final EditText height = (EditText) findViewById(R.id.input_height);
        final EditText age = (EditText) findViewById(R.id.input_age);
        final Button load = (Button) findViewById(R.id.btn_load);
        final TextView showLoad = (TextView) findViewById(R.id.view_data);

        calculate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    double a = Double.parseDouble(weight.getText().toString());
                    double b = Double.parseDouble(height.getText().toString()) / 100;
                    String tempAge = age.getText().toString();
                    if (a != 0 && b != 0 && a <= 150 && b <= 2.50) {
                        double c = Math.round((a / Math.pow(b, 2)) * 100);
                        c = c / 100;
                        System.out.println(c);
                        String finalresult = Double.toString(c);
                        Intent intent = new Intent(getApplicationContext(), OutputActivityChild.class);
                        intent.putExtra("Result", finalresult);
                        intent.putExtra("Age", tempAge);
                        if (male.isChecked()) {
                            intent.putExtra("Gender", male.getText());
                            startActivity(intent);
                        } else if (female.isChecked()) {
                            intent.putExtra("Gender", female.getText());
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.select, Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), R.string.not_a_value, Toast.LENGTH_SHORT).show();
                    }
                } catch (NullPointerException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), R.string.input_number, Toast.LENGTH_SHORT).show();
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    Toast.makeText(getApplicationContext(), R.string.input_number, Toast.LENGTH_SHORT).show();
                }
            }
        });

        load.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences("MySaves", Activity.MODE_PRIVATE);
                String loadValue = sp.getString("key", "string");
                showLoad.setText(R.string.last_value + loadValue);
            }
        });

    }

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.radio_female:
                if (checked)
                    break;
            case R.id.radio_male:
                if (checked)

                    break;
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void setUpActionBar() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            ActionBar actionBar = getActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }
}

