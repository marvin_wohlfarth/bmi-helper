package wohlfarth.android.BMI_Rechner;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by marvin.wohlfarth on 04.04.2014.
 */
public class OutputActivityChild extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.output_result_layout_child);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        final TextView output = (TextView) findViewById(R.id.output);
        final TextView overview = (TextView) findViewById(R.id.overview);
        final Button save = (Button) findViewById(R.id.btn_save);

        Intent i = getIntent();
        final String outputResult = i.getStringExtra("Result");
        System.out.println(outputResult);
        output.setText(R.string.output_bmi + outputResult);
        String gender = i.getStringExtra("Gender");
        final String ageTemp = i.getStringExtra("Age");
        final int age = Integer.parseInt(ageTemp);

        if (gender.equals("weiblich") || gender.equals("female")) {
            double bmiWert = Double.parseDouble(outputResult);
            if (age < 8) {
                overview.setText("Für Kinder unter 8 Jahren können leider keine zuverlässigen Aussagen getroffen werden.");
            } else {
                switch (age) {
                    case 8:
                        if (bmiWert < 13.2) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 13.2 && bmiWert < 15.9) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 15.9 && bmiWert < 18.8) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 18.8 && bmiWert < 22.3) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 9:
                        if (bmiWert < 13.7) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 13.7 && bmiWert < 16.4) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 16.4 && bmiWert < 19.8) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 19.8 && bmiWert < 23.4) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 10:
                        if (bmiWert < 14.2) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 14.2 && bmiWert < 16.9) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 16.9 && bmiWert < 20.7) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 20.7 && bmiWert < 23.4) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 11:
                        if (bmiWert < 14.6) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 14.6 && bmiWert < 17.7) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 17.7 && bmiWert < 20.8) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 20.8 && bmiWert < 22.9) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 12:
                        if (bmiWert < 16.0) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 16.0 && bmiWert < 18.4) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 18.4 && bmiWert < 21.5) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 21.5 && bmiWert < 23.4) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 13:
                        if (bmiWert < 15.6) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 15.6 && bmiWert < 18.9) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 18.9 && bmiWert < 22.1) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 22.1 && bmiWert < 24.4) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 14:
                        if (bmiWert < 17.0) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 17.0 && bmiWert < 19.4) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 19.4 && bmiWert < 23.2) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 23.2 && bmiWert < 26.0) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 15:
                        if (bmiWert < 17.6) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 17.6 && bmiWert < 20.2) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 20.2 && bmiWert < 23.2) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 23.2 && bmiWert < 27.6) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 16:
                        if (bmiWert < 17.8) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 17.8 && bmiWert < 20.3) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 20.3 && bmiWert < 22.8) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 22.8 && bmiWert < 24.2) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 17:
                        if (bmiWert < 17.8) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 17.8 && bmiWert < 20.5) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 20.5 && bmiWert < 23.4) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 23.4 && bmiWert < 25.7) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 18:
                        if (bmiWert < 18.3) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 18.3 && bmiWert < 20.6) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 20.6 && bmiWert < 23.5) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 23.5 && bmiWert < 25.0) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    default:
                        overview.setText("Bitte den BMI-Rechner für Erwachsene benutzen.");
                        break;
                }
            }
        } else {
            double bmiWert = Double.parseDouble(outputResult);
            if (age < 8) {
                overview.setText("Für Kinder unter 8 Jahren können leider keine zuverlässigen Aussagen getroffen werden.");
            } else {
                switch (age) {
                    case 8:
                        if (bmiWert < 14.2) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 14.2 && bmiWert < 16.4) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 16.4 && bmiWert < 19.3) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 19.3 && bmiWert < 22.6) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 9:
                        if (bmiWert < 13.7) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 13.7 && bmiWert < 17.1) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 17.1 && bmiWert < 19.4) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 19.4 && bmiWert < 21.6) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 10:
                        if (bmiWert < 14.6) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 14.6 && bmiWert < 17.1) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 17.1 && bmiWert < 21.4) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 21.4 && bmiWert < 25.0) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 11:
                        if (bmiWert < 14.3) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 14.3 && bmiWert < 17.8) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 17.8 && bmiWert < 21.2) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 21.2 && bmiWert < 23.1) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 12:
                        if (bmiWert < 14.8) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 14.8 && bmiWert < 18.4) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 18.4 && bmiWert < 22.0) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 22.0 && bmiWert < 24.8) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 13:
                        if (bmiWert < 16.2) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 16.2 && bmiWert < 19.1) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 19.1 && bmiWert < 21.7) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 21.7 && bmiWert < 24.5) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 14:
                        if (bmiWert < 16.7) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 16.7 && bmiWert < 19.8) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 19.8 && bmiWert < 22.6) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 22.6 && bmiWert < 25.7) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 15:
                        if (bmiWert < 17.8) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 17.8 && bmiWert < 20.2) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 20.2 && bmiWert < 23.1) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 23.1 && bmiWert < 25.9) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 16:
                        if (bmiWert < 18.5) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 18.5 && bmiWert < 21.0) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 21.0 && bmiWert < 23.7) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 23.7 && bmiWert < 26.0) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 17:
                        if (bmiWert < 18.6) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 18.6 && bmiWert < 21.6) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 21.6 && bmiWert < 23.7) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 23.7 && bmiWert < 25.8) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    case 18:
                        if (bmiWert < 18.6) {
                            overview.setText("starkes Untergewicht");
                        } else if (bmiWert >= 18.6 && bmiWert < 21.8) {
                            overview.setText("Untergewicht");
                        } else if (bmiWert >= 21.8 && bmiWert < 24.0) {
                            overview.setText("Normalgewicht");
                        } else if (bmiWert >= 24.0 && bmiWert < 26.8) {
                            overview.setText("Übergewicht");
                        } else {
                            overview.setText("starkes Übergewicht");
                        }
                        break;

                    default:
                        overview.setText("Bitte den BMI-Rechner für Erwachsene benutzen.");
                        break;
                }
            }
        }

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sp = getSharedPreferences("MySaves", Activity.MODE_PRIVATE);
                SharedPreferences.Editor editor = sp.edit();
                editor.putString("key", outputResult);
                editor.commit();
                Toast.makeText(getApplicationContext(), R.string.save_value, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menubar_activity, menu);
        return super.onCreateOptionsMenu(menu);
    }
}