package wohlfarth.android.BMI_Rechner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * Created by marvin.wohlfarth on 03.04.2014.
 */
public class MainMenu extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);

        final Button child = (Button) findViewById(R.id.btn_under_19);
        final Button adult = (Button) findViewById(R.id.btn_above_19);
        final Button exit = (Button) findViewById(R.id.btn_exit);

        child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivityChild.class);
                startActivity(intent);
            }
        });

        adult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivityAdult.class);
                startActivity(intent);
            }
        });

        exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                System.exit(0);
            }
        });

    }

    private int clickCount = 0;

    @Override
    public void onBackPressed() {
        if (clickCount == 1) {
            super.onBackPressed();
        } else {
            clickCount++;
            Toast.makeText(getApplicationContext(), R.string.on_Back_Pressed, Toast.LENGTH_SHORT).show();
        }
    }

}
