# **BMI-Rechner**

Diese Android-App ist beim Berechnen des BMI-Wertes hilfreich!

#### Anforderungen
- Android-Version: mind. 4.0 (Ice Cream Sandwich)

#### Erfolgreich getestet mit den folgenden Geräten:
- Samsung Galaxy S2
- Samsung Galaxy S3 mini
- Samsung Galaxy Fame
- Samsung Galaxy J5
- Samsung Galaxy Tab 3
- Oneplus One
- Oneplus X

##### Download
- Google Playstore: https://play.google.com/store/apps/details?id=wohlfarth.android.BMI_Rechner
- Bitbucket direkt: https://bitbucket.org/marvin_wohlfarth/bmi-helper/downloads
